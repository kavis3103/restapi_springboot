package com.adf.SpringBootApplication.Entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * BANK ACCOUNT ENTITY
 */
@Entity
public class BankAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int accountNumber;

	private String accountHolderName;

	private Date dob;

	private String accountType;

	private double transactionFee;

	private double balance;

	private Date createdAt;

	private Date updatedAt;
	
	@JsonIgnore
	@OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Transaction> transactions = new HashSet<>();

	/**
	 * @return the accountNumber
	 */
	public int getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the accountHolderName
	 */
	public String getAccountHolderName() {
		return accountHolderName;
	}

	/**
	 * @param accountHolderName the accountHolderName to set
	 */
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * @return the transactionFee
	 */
	public double getTransactionFee() {
		return transactionFee;
	}

	/**
	 * @param transactionFee the transactionFee to set
	 */
	public void setTransactionFee(double transactionFee) {
		this.transactionFee = transactionFee;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the transactions
	 */
	public Set<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	/**
	 * @param accountNumber
	 * @param accountHolderName
	 * @param dob
	 * @param accountType
	 * @param transactionFee
	 * @param balance
	 * @param createdAt
	 * @param updatedAt
	 * @param transactions
	 */
	public BankAccount(int accountNumber, String accountHolderName, Date dob, String accountType, double transactionFee,
			double balance, Date createdAt, Date updatedAt, Set<Transaction> transactions) {
		super();
		this.accountNumber = accountNumber;
		this.accountHolderName = accountHolderName;
		this.dob = dob;
		this.accountType = accountType;
		this.transactionFee = transactionFee;
		this.balance = balance;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.transactions = transactions;
	}

	/**
	 * 
	 */
	public BankAccount() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}