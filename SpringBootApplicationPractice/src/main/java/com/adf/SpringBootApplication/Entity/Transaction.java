package com.adf.SpringBootApplication.Entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * TRANSACTION ENTITY
 */
@Entity
public class Transaction {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("transaction_id")
	private int transactionId;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountNumber")
	@JsonIgnore
	private BankAccount bankAccount;

	@JsonProperty("amount")
	private double amount;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("old_balance")
	private double oldBalance;
	
	@JsonProperty("new_balance")
	private double newBalance;
	
	@JsonProperty("transaction_date")
	private Date transactionDate;
	
	@JsonProperty("transaction_status")
    private String transactionStatus;
	
	@JsonProperty("created_at")
    private Date createdAt;
	
	@JsonProperty("updated_at")
    private Date updatedAt;
	
	
	/**
	 * 
	 */
	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @param transactionId
	 * @param bankAccount
	 * @param amount
	 * @param type
	 * @param oldBalance
	 * @param newBalance
	 * @param transactionDate
	 * @param transactionStatus
	 * @param createdAt
	 * @param updatedAt
	 */
	public Transaction(int transactionId, BankAccount bankAccount, double amount, String type, double oldBalance,
			double newBalance, Date transactionDate, String transactionStatus, Date createdAt, Date updatedAt) {
		super();
		this.transactionId = transactionId;
		this.bankAccount = bankAccount;
		this.amount = amount;
		this.type = type;
		this.oldBalance = oldBalance;
		this.newBalance = newBalance;
		this.transactionDate = transactionDate;
		this.transactionStatus = transactionStatus;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the transactionId
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the bankAccount
	 */
	public BankAccount getBankAccount() {
		return bankAccount;
	}

	/**
	 * @param bankAccount the bankAccount to set
	 */
	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the oldBalance
	 */
	public double getOldBalance() {
		return oldBalance;
	}

	/**
	 * @param oldBalance the oldBalance to set
	 */
	public void setOldBalance(double oldBalance) {
		this.oldBalance = oldBalance;
	}

	/**
	 * @return the newBalance
	 */
	public double getNewBalance() {
		return newBalance;
	}

	/**
	 * @param newBalance the newBalance to set
	 */
	public void setNewBalance(double newBalance) {
		this.newBalance = newBalance;
	}

	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the transactionStatus
	 */
	public String getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	
    
	
    
}
