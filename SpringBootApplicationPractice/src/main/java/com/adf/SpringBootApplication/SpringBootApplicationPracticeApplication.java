package com.adf.SpringBootApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * main
 */
@SpringBootApplication
public class SpringBootApplicationPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApplicationPracticeApplication.class, args);
	}

}
