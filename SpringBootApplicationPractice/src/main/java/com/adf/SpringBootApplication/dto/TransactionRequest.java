package com.adf.SpringBootApplication.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
/*
 * Request object for do transaction
 */
public class TransactionRequest {

	
	public TransactionRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Min(1)
	@NotNull
	private double amount;
	
	@NotNull
	@Pattern(regexp = "WITHDRAWAL|DEPOSIT")
	private String type;
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
