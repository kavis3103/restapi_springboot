package com.adf.SpringBootApplication.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import com.adf.SpringBootApplication.exception.DateFormatException;
import com.fasterxml.jackson.annotation.JsonAlias;
/**
 * Request object for createAccount
 */
public class CreateAccountRequest {

	
	public CreateAccountRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	@NotNull
	@JsonAlias("account_holder_name")
	private String accountHolderName;
	
	@NotNull
	@Past
	private Date dob;
	
	@NotNull
	@Pattern(regexp = "SAVINGS|CURRENT")
	@JsonAlias("account_type")
	private String accountType;
	
	@JsonAlias("initial_deposit")
	private double initialDeposit;
	
	public String getAccountHolderName() {
		return accountHolderName;
	}
	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(String dob) {
		try {
			this.dob = new SimpleDateFormat("yyyy-MM-dd").parse(dob);
		} catch (ParseException e) {
			// making as future date
			throw new DateFormatException("Incorrect Date format");
			
		} 
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public double getInitialDeposit() {
		return initialDeposit;
	}
	public void setInitialDeposit(double initialDeposit) {
		this.initialDeposit = initialDeposit;
	}
}
