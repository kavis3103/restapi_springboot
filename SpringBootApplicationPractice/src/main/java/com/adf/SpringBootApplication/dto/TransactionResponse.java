package com.adf.SpringBootApplication.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
/*
 * Response object for do transaction
 */
public class TransactionResponse {
	
	public TransactionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("transaction_id")
	private int transactionId;
	
	@JsonProperty("account_number")
	private int accountNumber;

	@JsonProperty("amount")
	private double amount;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("old_balance")
	private double oldBalance;
	
	@JsonProperty("new_balance")
	private double newBalance;
	
	@JsonProperty("transaction_date")
	private Date transactionDate;
	
	@JsonProperty("transaction_status")
    private String transactionStatus;
	
	@JsonProperty("created_at")
    private Date createdAt;
	
	@JsonProperty("updated_at")
    private Date updatedAt;
	
	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getOldBalance() {
		return oldBalance;
	}

	public void setOldBalance(double oldBalance) {
		this.oldBalance = oldBalance;
	}

	public double getNewBalance() {
		return newBalance;
	}

	public void setNewBalance(double newBalance) {
		this.newBalance = newBalance;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
