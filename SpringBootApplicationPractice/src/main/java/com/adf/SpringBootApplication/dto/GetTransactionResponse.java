package com.adf.SpringBootApplication.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.adf.SpringBootApplication.Entity.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Response object for get transaction
 */
public class GetTransactionResponse {
	
	public GetTransactionResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("account_number")
	private int accountNumber;
	
	@JsonProperty("account_holder_name")
	private String accountHolderName;
	
	@JsonProperty("dob")
	private Date dob;
	
	@JsonProperty("account_type")
	private String accountType;
	
	@JsonProperty("balance")
	private double balance;
	
	@JsonProperty("from_date")
	private Date fromDate;
	
	@JsonProperty("to_date")
	private Date toDate;
	
	@JsonProperty("transactions")
	private List<Transaction> transactions;

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getDob() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dob);
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	

}
