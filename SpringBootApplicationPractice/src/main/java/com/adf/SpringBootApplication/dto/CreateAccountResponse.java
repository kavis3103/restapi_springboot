package com.adf.SpringBootApplication.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Response object for createAccount
 */
public class CreateAccountResponse {

	
	public CreateAccountResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("account_number")
	private int accountNumber;
	
	@JsonProperty("account_holder_name")
	private String accountHolderName;
	
	@JsonProperty("dob")
	private Date dob;
	
	@JsonProperty("account_type")
	private String accountType;
	
	@JsonProperty("transaction_fee")
	private double transactionFee;
	
	@JsonProperty("balance")
	private double balance;
	
	@JsonProperty("created_at")
	private Date createdAt;
	
	@JsonProperty("updated_at")
	private Date updatedAt;

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getDob() {
		return new SimpleDateFormat("dd/MM/yyyy").format(dob);
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getTransactionFee() {
		return transactionFee;
	}

	public void setTransactionFee(double transactionFee) {
		this.transactionFee = transactionFee;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
