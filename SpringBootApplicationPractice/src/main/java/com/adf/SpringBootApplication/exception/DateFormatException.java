package com.adf.SpringBootApplication.exception;

/**
 * DateFormatException
 */

public class DateFormatException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DateFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

}
