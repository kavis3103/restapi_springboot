package com.adf.SpringBootApplication.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * 
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class IDNotFoundException extends RuntimeException 
{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public IDNotFoundException(String message) 
{
super(message);
}
}