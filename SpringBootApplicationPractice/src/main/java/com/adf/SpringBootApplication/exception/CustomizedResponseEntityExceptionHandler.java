package com.adf.SpringBootApplication.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception handler
 */

@ControllerAdvice  
@RestController  
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	public final ResponseEntity<Object> handleAllExceptions(Exception exception, WebRequest request)  
	{  
		ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), exception.getLocalizedMessage(), request.getDescription(false));   
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);  
	} 
	
	public final ResponseEntity<Object> handleUserNotFoundExceptions(IDNotFoundException exception, WebRequest request)
	{
		ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), exception.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.NOT_FOUND);
	}
	
	public final ResponseEntity<Object> handleDateFormatException(DateFormatException exception, WebRequest request)
	{
		ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), exception.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	public final ResponseEntity<Object> handleMinimumBalanceExceptions(LessThanMinimumBalanceException exception, WebRequest request)
	{
		ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), exception.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) 
	{
		ExceptionResponse exceptionResponse= new ExceptionResponse(new Date(), "Validation Failed", exception.getBindingResult().toString());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

}
