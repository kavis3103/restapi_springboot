package com.adf.SpringBootApplication.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.adf.SpringBootApplication.Entity.Transaction;
import com.adf.SpringBootApplication.Entity.BankAccount;
import com.adf.SpringBootApplication.dto.CreateAccountRequest;
import com.adf.SpringBootApplication.dto.CreateAccountResponse;
import com.adf.SpringBootApplication.dto.GetTransactionRequest;
import com.adf.SpringBootApplication.dto.GetTransactionResponse;
import com.adf.SpringBootApplication.dto.TransactionRequest;
import com.adf.SpringBootApplication.dto.TransactionResponse;
import com.adf.SpringBootApplication.exception.IDNotFoundException;
import com.adf.SpringBootApplication.exception.LessThanMinimumBalanceException;
import com.adf.SpringBootApplication.repo.BankAccountRepo;
import com.adf.SpringBootApplication.repo.TransactionRepo;

/**
 * Controller class
 */
@RestController
public class BankRestController {
	
	public BankRestController() {
		super();
		// TODO Auto-generated constructor stub
	}

	Logger logger = LoggerFactory.getLogger(BankRestController.class);
	
	@Autowired
	BankAccountRepo accountRepo;
	
	@Autowired
	TransactionRepo transactionRepo;
	
	@PostMapping("account/create")
	public CreateAccountResponse createAccount(@Valid @RequestBody CreateAccountRequest request) {
		BankAccount account = new BankAccount();
		account.setAccountHolderName(request.getAccountHolderName());
		account.setAccountType(request.getAccountType());
		account.setDob(request.getDob());
		account.setBalance(request.getInitialDeposit());
		if(account.getAccountType().equals("CURRENT")) {
			account.setTransactionFee(5.0);
			logger.info("Creating current type account with 5.0 rs as transaction fee");
		}
		else {
			account.setTransactionFee(0.0);
			logger.info("Creating savings type account with 0.0 rs as transaction fee");
		}
		Date today = new Date();
		account.setCreatedAt(today);
		account.setUpdatedAt(today);
		accountRepo.save(account);
		logger.info("Account created...."+today);
		
		CreateAccountResponse response = new CreateAccountResponse();
		response.setAccountNumber(account.getAccountNumber());
		response.setAccountHolderName(account.getAccountHolderName());
		response.setDob(account.getDob());
		response.setAccountType(account.getAccountType());
		response.setBalance(account.getBalance());
		response.setTransactionFee(account.getTransactionFee());
		response.setCreatedAt(account.getCreatedAt());
		response.setUpdatedAt(account.getUpdatedAt());
		
		
		return response;
	}
	
	@PostMapping("account/{accountNumber}/transaction")
	public TransactionResponse doTransaction(@Valid @RequestBody TransactionRequest request, @PathVariable int accountNumber) {
		
		Transaction transaction = new Transaction();
		transaction.setAmount(request.getAmount());
		transaction.setType(request.getType());
		
		logger.info("Finding Your Account...");
		Optional<BankAccount> acco =accountRepo.findById(accountNumber);
		if(!acco.isPresent()) {
			logger.error("Account not found...");
			throw new IDNotFoundException(accountNumber+"not found");
		}
		BankAccount acc = acco.get();
		
		transaction.setBankAccount(acc);
		double oldBalance = acc.getBalance();
		logger.info("Your old balance"+ oldBalance);
		transaction.setOldBalance(oldBalance);
		transaction.setNewBalance(oldBalance);
		transaction.setTransactionStatus("FAILURE");
		double newBalance;
		if(transaction.getType().equals("WITHDRAWAL")) {
			newBalance = oldBalance - transaction.getAmount() - transaction.getBankAccount().getTransactionFee();
			logger.info("Withdrawing "+ transaction.getAmount() +" rs");
			if(newBalance < 0) {
				logger.error("Minimum balance....");
				throw new LessThanMinimumBalanceException("Maintain minimum balance");
			}
			transaction.setNewBalance(newBalance);
			logger.info("Your new balance"+ newBalance);
			transaction.getBankAccount().setBalance(newBalance);
			logger.info("Transaction Success...");
			transaction.setTransactionStatus("SUCCESS");
		}
		else if(transaction.getType().equals("DEPOSIT")) {
			newBalance = oldBalance + transaction.getAmount() - transaction.getBankAccount().getTransactionFee();
			logger.info("Depositing "+ transaction.getAmount() +" rs");
			transaction.setNewBalance(newBalance);
			logger.info("Your new balance"+ newBalance);
			transaction.getBankAccount().setBalance(newBalance);
			transaction.setTransactionStatus("SUCCESS");
			logger.info("Transaction Success...");
		}
		if(transaction.getTransactionStatus().equals("FAILURE")) {
			logger.info("Transaction Failure...");
		}
		Date today = new Date();
		transaction.setTransactionDate(today);
		transaction.setCreatedAt(today);
		transaction.setUpdatedAt(today);
		transaction.getBankAccount().setUpdatedAt(today);
		transactionRepo.save(transaction);
		logger.info("Transaction done..."+today);
		
		TransactionResponse response = new TransactionResponse();
		
		response.setTransactionId(transaction.getTransactionId());
		response.setAccountNumber(accountNumber);
		response.setAmount(transaction.getAmount());
		response.setType(transaction.getType());
		response.setOldBalance(transaction.getOldBalance());
		response.setNewBalance(transaction.getNewBalance());
		response.setTransactionDate(transaction.getTransactionDate());
		response.setTransactionStatus(transaction.getTransactionStatus());
		response.setCreatedAt(transaction.getCreatedAt());
		response.setUpdatedAt(transaction.getUpdatedAt());
		return response;	
	}
	
	@GetMapping("account/{accountNumber}/transaction/get")
	public GetTransactionResponse getTransaction(@RequestBody GetTransactionRequest request,@PathVariable int accountNumber) {
		logger.info("Finding Your Account...");
		Optional<BankAccount> acco =accountRepo.findById(accountNumber);
		if(!acco.isPresent()) {
			logger.error("Account not found...");
			throw new IDNotFoundException(accountNumber+"not found");
		}
		BankAccount account = acco.get();
		Date fromDate;
		if(request.getFromDate() == null) {
			fromDate = account.getCreatedAt();
		}
		else {
			fromDate = request.getFromDate();
		}
		Date toDate;
		if(request.getToDate() == null) {
			toDate = new Date();
		}
		else {
			toDate = request.getToDate();
		}
		logger.info("Getting Transaction Histry...");
		List<Transaction> list = transactionRepo.findAllTransactionsof(accountNumber, fromDate, toDate);
		
		GetTransactionResponse response = new GetTransactionResponse();
		
		response.setAccountNumber(account.getAccountNumber());
		response.setAccountHolderName(account.getAccountHolderName());
		response.setDob(account.getDob());
		response.setAccountType(account.getAccountType());
		response.setFromDate(fromDate);
		response.setToDate(toDate);
		response.setBalance(account.getBalance());
		response.setTransactions(list);
		
		return response;
	}

}
