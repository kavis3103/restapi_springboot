package com.adf.SpringBootApplication.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adf.SpringBootApplication.Entity.BankAccount;
/**
 * JPA Repository for BankAccount Entity
 */
public interface BankAccountRepo extends JpaRepository<BankAccount, Integer> {
	
}
