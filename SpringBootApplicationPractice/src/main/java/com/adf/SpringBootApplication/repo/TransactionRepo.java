package com.adf.SpringBootApplication.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adf.SpringBootApplication.Entity.Transaction;

/**
 * JPA Repository for Transaction Entity
 */
public interface TransactionRepo extends JpaRepository<Transaction, Integer> {
	
	/**
	 * 
	 * @param accountNumber
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	@Query("from Transaction where bankAccount.accountNumber = ?1 and transactionDate between ?2 and ?3")
	List<Transaction> findAllTransactionsof(int accountNumber, Date fromDate, Date toDate);

}
